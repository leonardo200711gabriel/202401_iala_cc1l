#include "pch.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;
using namespace System;
int generarAleatorio(int min, int max);

//Implementar funcion que permita yb arreglo aleatorio entre 100 y 500 y elementos
// aleatorios entre 1 y 10000. incluya codigo de prueba

int main()
{
    srand(time(nullptr));
        const int tam = generarAleatorio(100, 500);
        int* arreglo = new int[tam];
      
        for (int i = 0; i < tam; ++i) {

            arreglo[i] = generarAleatorio(1, 10000);
        }
        //imprimir arreglo
        for (int i = 0; i < tam; ++i) {
            cout << arreglo[i] << " ";
        }
        cout << endl;
        delete[] arreglo;
    return 0;
}

int generarAleatorio(int min, int max) {
    return min + rand() % (max - min + 1);
}